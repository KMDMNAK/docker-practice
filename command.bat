REM 停止中のcontainerを削除
docker system prunes

docker build -t {imagename} .

docker run -it -d --name {containerName} imagename

REM 起動中のcontainerのconsoleにアクセス
docker exec -it {containerName} {command}

REM VMのipを取得
docker-machine ip default
